package main

import (
	"flag"
	log "github.com/Sirupsen/logrus"
	"github.com/vharitonsky/iniflags"
	"gitlab.com/geomyidae/ws2811client"
	"gitlab.com/telecom-tower/pixtrix"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	columns           = 128
	rows              = 8
	defaultBrightness = 32
)

type flake struct {
	pos    int
	height int
	delay  int
	color  uint32
}

func makeSnow(ws *ws2811client.Ws2811Client, rows int, columns int) {

	frames := make([]*pixtrix.Pixtrix, 3)

	for i := range frames {
		frames[i] = &pixtrix.Pixtrix{
			Rows:   rows,
			Bitmap: make([]uint32, rows*columns),
		}
	}

	frameNr := make(chan int, 3)
	flakes := make([]flake, columns)

	go func() {
		i := 0
		for {
			// generate k flakes
			for k := 0; k < 2; k++ {
				x := rand.Intn(columns)
				if flakes[x].height == 0 {
					flakes[x].height = 2 + rand.Intn(2)
					flakes[x].pos = 0
					flakes[x].delay = 2 + rand.Intn(4)
					rg := 255 - 32 + uint32(rand.Intn(32))
					flakes[x].color = rg<<16 + rg<<8 + rg
				}
			}

			for x := 0; x < columns; x++ {
				for y := 0; y < rows; y++ {
					if y <= flakes[x].pos && y > flakes[x].pos-flakes[x].height {
						frames[i].SetPixel(x, y, flakes[x].color)
					} else {
						frames[i].SetPixel(x, y, 0x000000)
					}
				}
				if flakes[x].height > 0 {
					flakes[x].pos++
					if flakes[x].pos-flakes[x].height-flakes[x].delay > rows {
						flakes[x].pos = 0
						flakes[x].height = 0
						flakes[x].delay = 0
					}
				}
			}
			frameNr <- i
			i = (i + 1) % 3
		}
	}()

	for x := 0; x < columns; x++ {
		frames[0].SetPixel(x, 2, 0xffffff)
	}

	go func() {
		for {
			i := <-frameNr
			err := ws.Update(frames[i].InterleavedStripes()[0])
			if err != nil {
				os.Exit(0)
			}
			time.Sleep(50 * time.Millisecond)
		}
	}()

}

func main() {
	var brightness = flag.Int(
		"brightness", defaultBrightness,
		"Brightness between 0 and 255.")
	var debug = flag.Bool("debug", false, "be verbose")

	iniflags.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Infoln("Starting snowing tower")
	ws := ws2811client.LocalClient()
	ws.Brightness = *brightness
	ws.Open(1000 * time.Millisecond)

	makeSnow(ws, rows, columns)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs

	log.Infoln("Main loop terminated!")
	ws.Close()
}
